﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW3Hiearchical
{
    public partial class MainPage : ContentPage
    {
        //used to show what page was visited
        //this is called from the pages disappearing function
        public void Setword(string passed)
        {
            Checking.Text = passed;
        }
        //These are used to save the state of the first page
        int aaState = 0, abState = 0, acState = 0, baState = 0, bbState = 0, bcState = 0, caState = 0, cbState = 0, ccState = 0;
        public void FillState(int aa, int ab, int ac, int ba, int bb, int bc, int ca, int cb, int cc)
            {
            aaState = aa;
            abState = ab;
            acState = ac;
            baState = ba;
            bbState = bb;
            bcState = bc;
            caState = ca;
            cbState = cb;
            ccState = cc;
            }
       public (int,int,int,int,int,int,int,int ,int) SendBackState()
        {
            return (aaState, abState, acState, baState, bbState, bcState, caState, cbState, ccState);
        }
        //Saving the state of the second page
        int Page2State=0;
        public int SendBackPage2State()
        {
            return Page2State;
        }
        public void SavePage2State(int saved)
        {
            Page2State = saved;
        }
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);


        }
        async void FirstPageNav(object sender, EventArgs e)
         {
            await Navigation.PushAsync(new Page1(this));
         }
        async void SecondPageNav(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2(this));
        }
        async void ThirdPageNav(object sender,EventArgs e)
        {
            await Navigation.PushAsync(new Page3(this));
        }
    }
    
}
