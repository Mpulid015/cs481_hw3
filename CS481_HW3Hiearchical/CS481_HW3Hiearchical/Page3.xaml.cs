﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3Hiearchical
{   
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3 : ContentPage
	{
        MainPage mp;
		public Page3 (MainPage mainPage)
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, true);
            mp = mainPage;
        }
        //This will be called when the right answer is pressed
        //will go to the next page
        async void ExtraPageclick(object sender,EventArgs e)
        {
            await DisplayAlert("Good Job", "Now on to the second question", "Ok");
            await Navigation.PushAsync(new Page3Extra());
        }
        void Handle_appearing(object sender, EventArgs e)
        {

        }
        void Handle_disapearing(object sender, EventArgs e)
        {
            mp.Setword("Last visited: Page 3");
        }
        //This will change the button to red when it is clicked
        void WrongAns(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.BackgroundColor = Xamarin.Forms.Color.Red;

        }

    }
}