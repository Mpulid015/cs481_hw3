﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3Hiearchical
{
	
	public partial class Page1 : ContentPage
	{   //To get the main page reference
        MainPage mp;
        //These are used to save the states each button is in
        int aaState =0, abState=0, acState=0, baState=0, bbState=0, bcState=0, caState=0, cbState=0, ccState = 0;
		public Page1 (MainPage mainPage)
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, true);
            mp = mainPage;
		}
        /// <summary>
        /// This will decide what state Page 1 is in
        /// </summary>
        
        void Handle_appearing(object sender, EventArgs e)
        {
            var sentStates = mp.SendBackState();
            aaState = sentStates.Item1;
            abState = sentStates.Item2;
            acState = sentStates.Item3;
            baState = sentStates.Item4;
            bbState = sentStates.Item5;
            bcState = sentStates.Item6;
            caState = sentStates.Item7;
            cbState = sentStates.Item8;
            ccState = sentStates.Item9;
            switch (aaState)
            {
                case 0:
                    aa.Text = "";
                    break;
                case 1:
                    aa.Text = "X";
                    break;
                case 2:
                    aa.Text = "O";
                    break;
                default:
                    break;

            }
            switch (abState)
            {
                case 0:
                    ab.Text = "";
                    break;
                case 1:
                    ab.Text = "X";
                    break;
                case 2:
                    ab.Text = "O";
                    break;
                default:
                    break;

            }
            switch (acState)
            {
                case 0:
                    ac.Text = "";
                    break;
                case 1:
                    ac.Text = "X";
                    break;
                case 2:
                    ac.Text = "O";
                    break;
                default:
                    break;

            }
            switch (baState)
            {
                case 0:
                    ba.Text = "";
                    break;
                case 1:
                    ba.Text = "X";
                    break;
                case 2:
                    ba.Text = "O";
                    break;
                default:
                    break;

            }
            switch (bbState)
            {
                case 0:
                    bb.Text = "";
                    break;
                case 1:
                    bb.Text = "X";
                    break;
                case 2:
                    bb.Text = "O";
                    break;
                default:
                    break;

            }
            switch (bcState)
            {
                case 0:
                    bc.Text = "";
                    break;
                case 1:
                    bc.Text = "X";
                    break;
                case 2:
                    bc.Text = "O";
                    break;
                default:
                    break;

            }
            switch (caState)
            {
                case 0:
                    ca.Text = "";
                    break;
                case 1:
                    ca.Text = "X";
                    break;
                case 2:
                    ca.Text = "O";
                    break;
                default:
                    break;

            }
            switch (cbState)
            {
                case 0:
                    cb.Text = "";
                    break;
                case 1:
                    cb.Text = "X";
                    break;
                case 2:
                    cb.Text = "O";
                    break;
                default:
                    break;

            }
            switch (ccState)
            {
                case 0:
                    cc.Text = "";
                    break;
                case 1:
                    cc.Text = "X";
                    break;
                case 2:
                    cc.Text = "O";
                    break;
                default:
                    break;

            }


        }
        //This will save the state of each button so that if the page is opened again it can set itself back
        void Handle_disapearing(object sender,EventArgs e)
        {
            mp.Setword("Last visited: Page 1");
            mp.FillState(aaState, abState,acState,baState,bbState,bcState,caState,cbState,ccState);
        }
        //This next set of functions are for each button to have decide what 
        //state it is in and to change the text between black, X or O
        void AAchange(object sender,EventArgs e)
        {
            Button button = (Button)sender;

            if (aaState == 0)
            {
                button.Text = "X";
                aaState = 1;

            }
            else if (aaState == 1)
            {
                button.Text = "O";
                aaState = 2;
            }
            else if (aaState == 2)
            {
                button.Text = "";
                aaState = 0;
            }
          
        }

        void ABchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (abState == 0)
            {
                button.Text = "X";
                abState = 1;
            }
            else if (abState == 1)
            {
                button.Text = "O";
                abState = 2;
            }
            else if (abState == 2)
            {
                button.Text = "";
                abState = 0;
            }

        }
        void ACchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (acState == 0)
            {
                button.Text = "X";
                acState = 1;
            }
            else if (acState == 1)
            {
                button.Text = "O";
                acState = 2;
            }
            else if (acState == 2)
            {
                button.Text = "";
                acState = 0;
            }

        }
        void BAchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (baState == 0)
            {
                button.Text = "X";
                baState = 1;
            }
            else if (baState == 1)
            {
                button.Text = "O";
                baState = 2;
            }
            else if (baState == 2)
            {
                button.Text = "";
                baState = 0;
            }

        }
        void BBchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (bbState == 0)
            {
                button.Text = "X";
                bbState = 1;
            }
            else if (bbState == 1)
            {
                button.Text = "O";
                bbState = 2;
            }
            else if (bbState == 2)
            {
                button.Text = "";
                bbState = 0;
            }

        }
        void BCchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (bcState == 0)
            {
                button.Text = "X";
                bcState = 1;
            }
            else if (bcState == 1)
            {
                button.Text = "O";
                bcState = 2;
            }
            else if (bcState == 2)
            {
                button.Text = "";
                bcState = 0;
            }

        }
        void CAchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (caState == 0)
            {
                button.Text = "X";
                caState = 1;
            }
            else if (caState == 1)
            {
                button.Text = "O";
                caState = 2;
            }
            else if (caState == 2)
            {
                button.Text = "";
                caState = 0;
            }

        }
        void CBchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (cbState == 0)
            {
                button.Text = "X";
                cbState = 1;
            }
            else if (cbState == 1)
            {
                button.Text = "O";
                cbState = 2;
            }
            else if (cbState == 2)
            {
                button.Text = "";
                cbState = 0;
            }

        }
        void CCchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (ccState == 0)
            {
                button.Text = "X";
                ccState = 1;
            }
            else if (ccState == 1)
            {
                button.Text = "O";
                ccState = 2;
            }
            else if (ccState == 2)
            {
                button.Text = "";
                ccState = 0;
            }

        }

    }
}