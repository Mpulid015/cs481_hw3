﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3Hiearchical
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3Extra : ContentPage
	{
		public Page3Extra ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, true);
        }
        //When the right answer is clicked and will go back to the main page with the PopToRootAsync
        async void GoHomeClick(object sender,EventArgs e)
        {
            await DisplayAlert("Congrats", "You got it right Now Lets go to the Main Page", "Ok");
            await Navigation.PopToRootAsync();
        }
        void Handle_appearing(object sender, EventArgs e)
        {

        }
        void Handle_disapearing(object sender, EventArgs e)
        {

        }
        //Changes the color of the button 
        void WrongAns(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.BackgroundColor = Xamarin.Forms.Color.Red;

        }
    }
}