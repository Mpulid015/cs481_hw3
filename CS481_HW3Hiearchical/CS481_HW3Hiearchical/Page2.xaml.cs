﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3Hiearchical
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{   //Used to save what album was being looked at after leaving
        int AlbumState = 0;
        //To get the reference of the main page
        MainPage mp;
		public Page2 (MainPage mainPage)
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, true);
            mp = mainPage;
        }
        //This will state the state the page was in when it is opened
        void Handle_appearing(object sender, EventArgs e)
        {
            AlbumState=mp.SendBackPage2State();
            if(AlbumState == 0)
            {//Image from https://en.wikipedia.org/wiki/Kill_%27Em_All
                Album.Source = "MetallicaKill.jpg";
                AlbumDetail.Text = "Released 1982\nList of Songs\nHit The Lights\nThe Four Horse Men\n Motorbreath\nJump In The Fire\n(Anesthesia)--Pulling Teeth" +
                    "\nWhiplash/nPhantom Lord\nNo Remorse\n Seek & Destroy\nMetal Milita ";
            }
            else if(AlbumState ==1)
            {//image from https://en.wikipedia.org/wiki/Ride_the_Lightning
                Album.Source = "Ride.png";
                AlbumDetail.Text = "Released 1984 \n List of Songs \n Fight Fire With Fire \nRide The Lightning\nFor Whom The Bell Tolls\nFade To Black" +
                    "\nTrapped Under Ice \nEscape \nCreeping Death \nThe Call Of Ktulu";
            }
            else if(AlbumState ==2)
            {//Image from https://en.wikipedia.org/wiki/Master_of_Puppets
                Album.Source = "MetallicaMaster.jpg";
                AlbumDetail.Text = "Released 1986\nList of Songs\nBattery\nMaster of Puppets\nThe Thing That Should Not Be" +
                    "\nWelcome Home\nDisposable Heroes\nLeper Messiah\nOrionDamage,Inc.";
            }
        }
        //saves the state of which album was being looked at
        void Handle_disapearing(object sender, EventArgs e)
        {
            mp.Setword("Last visited: Page 2");
            mp.SavePage2State(AlbumState);
        }
        //Next set of buttons are to switch around the albums the user wants to see
        void RideButton (object sender,EventArgs e)
        {   if (AlbumState != 1)
            {
                Album.Source = "Ride.png";
                AlbumDetail.Text = "Released 1984 \n List of songs \n Fight Fire With Fire \nRide The Lightning\nFor Whom The Bell Tolls\nFade To Black" +
                    "\nTrapped Under Ice \nEscape \nCreeping Death \nThe Call Of Ktulu";
                AlbumState = 1;
            }
        }
        void Masterbutton(object sender,EventArgs e)
        {
            if (AlbumState != 2)
            {
                Album.Source = "MetallicaMaster.jpg";
                AlbumDetail.Text = "Released 1986\nList of Songs\nBattery\nMaster of Puppets\nThe Thing That Should Not Be" +
                        "\nWelcome Home\nDisposable Heroes\nLeper Messiah\nOrionDamage,Inc.";
                AlbumState = 2;
            }
        }
        void KillButton(object sender, EventArgs e)
        {
            if (AlbumState != 0)
            {
                Album.Source = "MetallicaKill.jpg";
                AlbumDetail.Text = "Released 1982\nList of Songs\nHit The Lights\nThe Four Horse Men\n Motorbreath\nJump In The Fire\n(Anesthesia)--Pulling Teeth" +
                        "\nWhiplash/nPhantom Lord\nNo Remorse\n Seek & Destroy\nMetal Milita ";
                AlbumState = 0;
            }
        }
    }
}